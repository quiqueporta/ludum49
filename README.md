# ludum49

![](https://upload.wikimedia.org/wikipedia/commons/0/03/New_Ludum_Dare_Logo.png)

Game for the Ludum Dare 49 (https://ldjam.com/events/ludum-dare/49)


## Resources

### Engine

https://www.godotengine.org/

https://docs.godotengine.org/en/stable/

### Graphics

https://kenney.nl/assets

https://www.piskelapp.com/

### Audio

http://www.drpetter.se/project_sfxr.html

https://sfxr.me/

### Game Hosting

https://itch.io

## License

This work © 2021 by Quique Porta is licensed under Attribution-NonCommercial-ShareAlike 4.0 International. To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/4.0/
