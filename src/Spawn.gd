extends Position2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _unhandled_input(event: InputEvent) -> void:
	if not event is InputEventMouseButton:
		return
		
	if event.button_index != BUTTON_LEFT or not event.pressed:
		return

	var enemy_scene = load("res://Enemy.tscn")
	var enemy = enemy_scene.instance()
	enemy.set_global_position(self.global_position)
	enemy.speed = rand_range(100, 100)
	get_parent().add_child(enemy)
	var nav_2d: Navigation2D = get_parent().get_node("LevelNavigation")
	var destination = get_parent().destination
	var new_path : = nav_2d.get_simple_path(enemy.global_position, destination, false)
	enemy.path = new_path
