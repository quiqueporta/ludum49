extends KinematicBody2D

var destination: Vector2 = Vector2.ZERO setget set_destination
var speed: int = 5


func set_destination(value: Vector2) -> void:
	destination = value


func _physics_process(delta):
	var collision: KinematicCollision2D = move_and_collide(destination.normalized() * delta * 300)
	if collision != null:
		if "Bullet" in collision.collider.name:
			return
		queue_free()
		print(collision.collider.name)
		collision.collider.hit(1)


func _on_VisibilityNotifier2D_screen_exited():
	queue_free()
