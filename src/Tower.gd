extends Node2D

export(int) var radius: int = 100
var bullet_scene = preload("res://Bullet.tscn")
# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var detected_enemies: Array = []


# Called when the node enters the scene tree for the first time.
func _ready():
	pass
	# $CollisionShape2D.shape

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	fire()


func _on_Range_body_entered(body) -> void:
	detected_enemies.append(body)
	
	
func fire() -> void:
	if detected_enemies.size() == 0:
		return
	var enemy = next_detected_enemy()
	if not enemy:
		return
	yield(get_tree().create_timer(1), "timeout")
	var bullet = bullet_scene.instance()
	bullet.set_global_position(global_position)
	if is_instance_valid(enemy) and bullet:
		var enemy_position = enemy.global_position
		var bullet_position = bullet.global_position
		bullet.destination = enemy_position - bullet_position
		get_parent().add_child(bullet)
	

func next_detected_enemy():
	var next = null
	for i in detected_enemies.size():
		next = detected_enemies[i]
		if is_instance_valid(next):
			return next
			break

	return next
